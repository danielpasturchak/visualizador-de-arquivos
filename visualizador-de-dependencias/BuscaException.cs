﻿using System;
using System.Runtime.Serialization;

namespace visualizador_de_dependencias
{
    [Serializable]
    internal class BuscaException : Exception
    {
        public BuscaException()
        {
        }

        public BuscaException(string message) : base(message)
        {
        }

        public BuscaException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BuscaException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}