﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using visualizador_de_dependencias.controllers;
using visualizador_de_dependencias.models;
using visualizador_de_dependencias_GUI;

namespace visualizador_de_dependencias
{
    class Program
    {
        static void Main(string[] args)
        {
            var HelloMessage = "\n\n\n" +
                "\tPor favor, digite algum argumento!\n\n" +
                "\t\t-G para abrir uma GUI com a visualização das dependencias.\n\n";

            if (args == null || args.Count() < 1)
            {
                Console.WriteLine(HelloMessage);
                return;
            }

            try
            {
                switch (args[0])
                {
                    case "-G":

                        var dependenciascontroller = new DependenciasController();
                        List<IArquivo> arquivos = dependenciascontroller.BuscarArquivos();
                        var gui = new MainWindow();
                        gui.CarregarArquivos(arquivos);
                        gui.Show();
                        break;

                    default:
                        Console.WriteLine("Argumento inválido");
                        return;
                }
            }
            catch (BuscaException be)
            {
                Console.WriteLine(be.Message);
            }
            catch(GUIException gex)
            {
                Console.WriteLine(gex.Message);
            }

        }
    }
}
