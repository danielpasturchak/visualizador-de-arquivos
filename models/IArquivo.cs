﻿using System.Collections.Generic;

namespace visualizador_de_dependencias.models
{
    public interface IArquivo
    {

        string Hash_ID { get; }
        string Extensao { get; }
        IArquivo [] Dependencias { get; set; }

        Stack<Pasta> Get_Localizacao(ReferenciaLocalizacao referencia); // Buscar nos arquivos algum arquivo que gera esta Hash e retornar a localização.
    }

    public enum ReferenciaLocalizacao
    {
        DaRaizDoProjeto = 1,
        DaRaizDoSO = 2
    }
}