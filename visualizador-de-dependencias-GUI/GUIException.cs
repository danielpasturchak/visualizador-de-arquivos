﻿using System;
using System.Runtime.Serialization;

namespace visualizador_de_dependencias_GUI
{
    [Serializable]
    public class GUIException : Exception
    {
        public GUIException()
        {
        }

        public GUIException(string message) : base(message)
        {
        }

        public GUIException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected GUIException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}